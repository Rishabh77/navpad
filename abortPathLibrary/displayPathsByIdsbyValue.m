function displayPathsByIdsbyValue(pathIds,globalPathList,globalPathIds,value)
%DISPLAYMAPVELOCITYPATHS Summary of this function goes here
%   Detailed explanation goes here
% preparing data for scatter
x =[];
y =[];
c =[];

colormap('Winter')

pathIds = sort(pathIds);
mid = round(0.5*size(pathIds,1));
for i=1:mid
    ids = globalPathIds(pathIds(i),:);
    pathxy = globalPathList(ids(1):ids(2),2:3);
    color = value(i)*ones(size(pathxy,1),1);
    %color = log((1-color)./color);
    x = [x;pathxy(:,1)];
    y = [y;pathxy(:,2)];
    c = [c;color];
end

for i=size(pathIds,1):-1:mid+1
    ids = globalPathIds(pathIds(i),:);
    pathxy = globalPathList(ids(1):ids(2),2:3);
    color = (value(i)*ones(size(pathxy,1),1));
    %color = log((1-color)./color);
    x = [x;pathxy(:,1)];
    y = [y;pathxy(:,2)];
    c = [c;color];
end

scatter(x,y,30,c,'filled')
end