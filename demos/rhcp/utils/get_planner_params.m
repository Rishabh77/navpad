function planner_params = get_planner_params( id )
%GET_PLANNER_PARAMS Summary of this function goes here
%   Detailed explanation goes here

planner_params = struct();

switch id
    case 1
        load ../../normalPathLibrary/gk_path_set.mat;
        planner_params.path_set = gk_path_set;                                  %Which path set
        planner_params.ind = round(length(planner_params.path_set(1).x)*0.1);   %Replan index
        planner_params.f_cost = @(path) sum(sqrt(diff(path.x).^2+diff(path.y).^2));            %Cost handle
        planner_params.f_heur = @get_value;                                                    %Heuristic handle
        planner_params.f_coll = @collision_check_binary;                                       %Collision check handle
        planner_params.j_coll = 100;
    case 2
        load ../../normalPathLibrary/gk_path_set.mat;
        planner_params.path_set = gk_path_set;                                  %Which path set
        planner_params.ind = round(length(planner_params.path_set(1).x)*0.05);   %Replan index
        planner_params.f_cost = @(path) sum(sqrt(diff(path.x).^2+diff(path.y).^2));            %Cost handle
        planner_params.f_heur = @get_value;                                                    %Heuristic handle
        planner_params.f_coll = @collision_check_binary;                                       %Collision check handle
        planner_params.j_coll = 1000;
    case 'info_graph'
        planner_params.path_resolution_time = 0.01;
        planner_params.path_time = 30;
        planner_params.ind = 5;   %Replan index
        planner_params.num_nodes = 10;
    otherwise
        error('Planner parameters requested not available')
end

end

