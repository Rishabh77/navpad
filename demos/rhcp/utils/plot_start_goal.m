function [ handles ] = plot_start_goal(start_coord_struct,goal_coord_struct,goal_radius,map_struct)
%PLOT_START_GOAL Summary of this function goes here
%   Detailed explanation goes here
start_coord = [start_coord_struct.x start_coord_struct.y];
goal_coord = [goal_coord_struct.x goal_coord_struct.y];
hold on
goal_coordId = coord2index(goal_coord(1),goal_coord(2),map_struct.scale,map_struct.min(1),map_struct.min(2));
start_coordId = coord2index(start_coord(1),start_coord(2),map_struct.scale,map_struct.min(1),map_struct.min(2));
handles(1) = plot(start_coordId(1),start_coordId(2),'r.','MarkerSize',10);
handles(2) = plot(goal_coordId(1),goal_coordId(2),'ro','MarkerSize',5);
theta = 0:0.1:2*pi;
x = goal_radius*cos(theta) + goal_coord(1);
y = goal_radius*sin(theta) + goal_coord(2);

xy = coord2indexNotRound(x',y',map_struct.scale,map_struct.min(1),map_struct.min(2));

handles(3) = plot(xy(:,1),xy(:,2),'r','LineWidth',3);
end