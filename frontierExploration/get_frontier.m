function [ frontier ] = get_frontier( grid_map_struct, grid_params, distance_map_struct, wavefront_params)
%GET_FRONTIER Summary of this function goes here
%   Detailed explanation goes here
data = grid_map_struct.data;
data(data>=grid_params.log_odds_occ) = 1;
data(data<grid_params.log_odds_occ) = 0;

data = edge(data);
ind = find(data>0);
ind(distance_map_struct.data(ind)<=wavefront_params.min_obstacle_distance)=[];
if (strcmp(wavefront_params.mode ,'obstacle'))
    ind(distance_map_struct.data(ind)>=wavefront_params.max_obstacle_distance)=[];
end
data(:)=0;
data(ind)=1;
[r, c] = ind2sub(size(grid_map_struct.data),ind);
[frontier(:,1),frontier(:,2)] = index2coord([r c],grid_map_struct.scale,grid_map_struct.min(1),grid_map_struct.min(2));
end

